<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @since      1.0.0
 *
 * @package    Forget_About_Shortcode_Buttons
 * @subpackage Forget_About_Shortcode_Buttons/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Forget_About_Shortcode_Buttons
 * @subpackage Forget_About_Shortcode_Buttons/public
 * @author     Ross <r@r.com>
 */
class Forget_About_Shortcode_Buttons_Public
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $plugin_name The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $version The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string $plugin_name The name of the plugin.
     * @param      string $version The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {
        date_default_timezone_set('Asia/Kuala_Lumpur');
        $this->plugin_name = $plugin_name;
        $this->version     = $version;
        add_action("wp_ajax_pn_price_tracking_save_email", array($this, "pn_price_tracking_save_email"));
        add_action("wp_ajax_nopriv_pn_price_tracking_save_email", array($this, "pn_price_tracking_save_email"));
    }


    /**
     * @return bool
     */
    public function pn_price_tracking_save_email()
    {
        global $wpdb;
        $email          = $_REQUEST['email'] == "" ? null : $_REQUEST['email'];
        $productName    = $_REQUEST['product_name'];
        $type           = $_REQUEST['type'];
        $url            = $_REQUEST['url'];
        $destinationUrl = $_REQUEST['destination_url'];

	    if($type !== 'click')
	    {
		    $wpdb->insert(
			    $wpdb->prefix . 'price_tracking_surveys',
			    array(
				    'produt_name'     => $productName,
				    'email'           => $email,
				    'type'            => $type,
				    'url'             => $url,
				    'destination_url' => $destinationUrl,
				    'created_at'      => date('Y-m-d H:i:s'),
				    'updated_at'      => date('Y-m-d H:i:s'),
			    )
		    );
	    }

	    $ip = $this->get_ip_address();

	    $tags = json_decode(file_get_contents('http://ip-api.com/json/' . $ip));
	    if(isset($tags->status) && $tags->status !== 'success'){
		    $tags = json_decode(file_get_contents('http://www.geoplugin.net/json.gp?ip=' . $ip));
		    $country = $tags->geoplugin_countryName ?? null;
	    } else {
		    $country = $tags->country ?? null;
	    }

	    if(null === $country){
		    $tags = json_decode(file_get_contents('http://api.hostip.info/get_json.php?ip=' . $ip));
		    $country = $tags->country_name ?? null;
	    }

	    $user_agent = $_SERVER['HTTP_USER_AGENT'];

	    require_once plugin_dir_path(__FILE__) . 'Browser.php';
	    $browser = new Browser();

	    $post_id = url_to_postid($url);
	    $term_lists = wp_get_post_terms($post_id, 'category');

	    $taxonomy_id = $term_lists[0]->term_id ?? 'null';

	    $wpdb->insert(
		    $wpdb->prefix . 'click_tracking',
		    array(
			    'post_id'         => $post_id,
			    'taxonomy_id'     => $taxonomy_id,
			    'ip_address'      => $ip,
			    'source'          => $url,
			    'destination_url' => $destinationUrl,
			    'country'         => $country,
			    'user_agent'      => $user_agent,
			    'browser_name'    => $browser->getBrowser(),
			    'browser_version' => $browser->getVersion(),
			    'device_os'       => $browser->getPlatform(),
			    'is_mobile'       => $browser->isMobile(),
			    'created_at'      => date('Y-m-d H:i:s'),
			    'updated_at'      => date('Y-m-d H:i:s'),
		    )
	    );

        return true;
    }

	public function get_ip_address() {
		// check for shared internet/ISP IP
		if (!empty($_SERVER['HTTP_CLIENT_IP']) && $this->validate_ip($_SERVER['HTTP_CLIENT_IP'])) {
			return $_SERVER['HTTP_CLIENT_IP'];
		}

		// check for IPs passing through proxies
		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			// check if multiple ips exist in var
			if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
				$iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
				foreach ($iplist as $ip) {
					if ($this->validate_ip($ip))
						return $ip;
				}
			} else {
				if ($this->validate_ip($_SERVER['HTTP_X_FORWARDED_FOR']))
					return $_SERVER['HTTP_X_FORWARDED_FOR'];
			}
		}
		if (!empty($_SERVER['HTTP_X_FORWARDED']) && $this->validate_ip($_SERVER['HTTP_X_FORWARDED']))
			return $_SERVER['HTTP_X_FORWARDED'];
		if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && $this->validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
			return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
		if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && $this->validate_ip($_SERVER['HTTP_FORWARDED_FOR']))
			return $_SERVER['HTTP_FORWARDED_FOR'];
		if (!empty($_SERVER['HTTP_FORWARDED']) && $this->validate_ip($_SERVER['HTTP_FORWARDED']))
			return $_SERVER['HTTP_FORWARDED'];

		// return unreliable ip since all else failed
		return $_SERVER['REMOTE_ADDR'];
	}

	/**
	 * Ensures an ip address is both a valid IP and does not fall within
	 * a private network range.
	 */
	public function validate_ip($ip) {
		if (strtolower($ip) === 'unknown')
			return false;

		// generate ipv4 network address
		$ip = ip2long($ip);

		// if the ip is set and not equivalent to 255.255.255.255
		if ($ip !== false && $ip !== -1) {
			// make sure to get unsigned long representation of ip
			// due to discrepancies between 32 and 64 bit OSes and
			// signed numbers (ints default to signed in PHP)
			$ip = sprintf('%u', $ip);
			// do private network range checking
			if ($ip >= 0 && $ip <= 50331647) return false;
			if ($ip >= 167772160 && $ip <= 184549375) return false;
			if ($ip >= 2130706432 && $ip <= 2147483647) return false;
			if ($ip >= 2851995648 && $ip <= 2852061183) return false;
			if ($ip >= 2886729728 && $ip <= 2887778303) return false;
			if ($ip >= 3221225984 && $ip <= 3221226239) return false;
			if ($ip >= 3232235520 && $ip <= 3232301055) return false;
			if ($ip >= 4294967040) return false;
		}
		return true;
	}

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Forget_About_Shortcode_Buttons_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Forget_About_Shortcode_Buttons_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/button-styles.css', array(), $this->version, 'all');
        wp_enqueue_style("pn-" . $this->plugin_name, plugin_dir_url(__FILE__) . 'css/pn.link.css', array(), $this->version, 'all'); // {Tag} PN Custom - add custom style for link-modal

    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Forget_About_Shortcode_Buttons_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Forget_About_Shortcode_Buttons_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        //wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/forget-about-shortcode-buttons-public.js', array( 'jquery' ), $this->version, false );
        wp_enqueue_script("pn-normal-link" . $this->plugin_name, plugin_dir_url(__FILE__) . 'js/pn.normal.link.js', array('jquery'), $this->version, false); // {Tag} PN Custom - add custom js for modal popup
        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/pn.link-popup.js', array('jquery'), $this->version, false); // {Tag} PN Custom - add custom js for modal popup
        wp_enqueue_script( "pn-param-link". $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/pn.param.link.js', array( 'jquery' ), $this->version, true ); // {Tag} PN Custom - add custom js for param link
        wp_localize_script($this->plugin_name, 'PN', array( 'param_tracker' => get_option( 'param_tracker' ) )); // {Tag} PN Custom - param_tracker
        wp_localize_script($this->plugin_name, 'pnAjax', array(
            'ajaxurl' => admin_url('admin-ajax.php'),
        ));

    }

    /**
     * {Tag} PN Custom
     * Modal popup template
     */
    public function wp_footer()
    {
        $this->text = array(
            'pn_link_modal_text' => wp_unslash(get_option('pn_link_modal_text')),
        );

        require_once plugin_dir_path(__FILE__) . 'partials/pn-link-modal-template.php';
        require_once plugin_dir_path(__FILE__) . 'partials/pn-price-tracking-modal-template.php';
    }

}
