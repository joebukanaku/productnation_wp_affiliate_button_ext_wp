(function( $ ) {

	'use strict';

	$(function(){
	  var params = [],
	      qs;
	  var pageURL = decodeURIComponent(window.location.search.substring(1));
	  var urlVars = pageURL.split('&');
	  var getSub = PN.param_tracker; // define the sub to use

	  $.each(urlVars,function(index,val){
	    var split_var = val.split('=');
	    params[split_var[0]] = split_var[1];
	  });

	  if(typeof params[getSub] === 'undefined')
	  {
	    return ; // skip script if no aff_sub4 defined
	  }

	  // var dismissUrl = window.location.host; // get current url host -- [deprecated]

	  // v2 -- implement params passing only for the following URL
	  var onlyHost = [
	        'invol.co',
	        'tracking.shopstylers.com',
	        'ho.lazada.com.my'
	    ];


	  $('a').each(function(){
	      qs = '?';

	      if( $.inArray(this.host,onlyHost) >= 0 ) // check if it link is in defined array
	      {
	        if(this.href.indexOf('?') >= 0){
	          qs = '&';
	        }

	        this.href = this.href + qs + getSub +"=" + encodeURIComponent(params[getSub]); // append the sub to url
	      }
	  });

	});
})( jQuery );
