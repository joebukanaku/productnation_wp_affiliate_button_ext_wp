/*
* {Tag} PN Custom
*	- custom js for popup modal
*/

(function( $ ) {

	'use strict';
	$(function(){
		// {Tag} PN Custom
		// v1.1 - if script can be executed, change the href to get value from data-href
		// only for mobile & desktop
		// not FB Instant Article & AMP
        var isM = isMobile();

        /**
         * parse button links and assign class for popup
         */
        function parseLinkstoAssignClass() {
            //var sub4 = $.urlParam('aff_sub_4');
            //var re = new RegExp(/https?:\/\/(invol\.co|tracking\.shopstylers\.com|ho\.lazada\.com\.my)/i);
            $('a.pn-fasc-link').each(function(){
                $(this).removeClass('pn-show-price-tracking-popup');
                $(this).removeClass('pn-mobile-link');
                var url = $(this).attr('data-href');
                $(this).attr('href',url);

                var isPriceTrackingSurvey = getCookie('price-tracking-survey');
                //var isPriceTrackingSurvey = false;
                if($(this).attr('data-is-price-tracking') == "true" && !isPriceTrackingSurvey)
                {
                    $(this).addClass('pn-show-price-tracking-popup');
                }
                else if(isM)
                {
                    $(this).addClass('pn-mobile-link'); // add mobile link class
                }
            });
        }

        /**
         * Send Ajax call
         * @param url
         * @param email
         * @param productName
         * @param type
         * @param sucessCallback
         */
        function sendAjaxToPriceTracking(url,destination_url,email, productName,type, successCallback)
        {
            $.ajax({
                type : "post",
                dataType : "json",
                url : url,
                data : {action: "pn_price_tracking_save_email", email : email, product_name: productName, type:type, 'url':window.location.href,destination_url:destination_url},
                success: function( data ) {
                    setCookie('price-tracking-survey',true,30);
                    if (typeof successCallback === "function") {
                        successCallback();
                    }
                }
            })
        }

        parseLinkstoAssignClass();

		//listen for price tracking button click
        $(document).on('click','.pn-fasc-link',function(e){
            var url = $(this).attr('href');
            var app_url = $(this).attr('data-app-href');
            if($(this).hasClass('pn-show-price-tracking-popup')) {
                e.preventDefault();
                $(".pn-price-tracking-modal-thanks-body").hide();
                $(".pn-price-tracking-modal-main-body").show();
                var productName = $(this).attr('data-product-name');
                var rel = $(this).attr('rel');
                var target = $(this).attr('target');
                if (app_url != '' && isM) {
                    url = app_url;
                }
                $('.pn-price-tracker-modal-product-name').html(productName);
                $.magnificPopup.open({
                    items: {
                        src: '#pn-price-tracking-modal',
                        type: 'inline'
                    },

                    fixedContentPos: false,
                    fixedBgPos: true,
                    overflowY: 'auto',
                    alignTop: true,
                    closeBtnInside: true,
                    preloader: false,
                    midClick: true,
                    removalDelay: 300,
                    mainClass: 'my-mfp-zoom-in',
                    callbacks: {
                        close: function () {
                            parseLinkstoAssignClass();
                            return true;
                        }
                    }
                });

                //send for opening popup
                //sendAjaxToPriceTracking(pnAjax.ajaxurl,null,productName,'open', function () {
                //});

                $("#pn-price-tracking-no").on('click', function () {
                    console.log("clicked no");
                    window.open(url);
                    sendAjaxToPriceTracking(pnAjax.ajaxurl, url, null, productName, 'no', function () {
                        $.magnificPopup.instance.close();
                    });
                });
                $("#pn-price-tracking-yes").on('click', function () {
                    console.log("clicked yes");
                    var email = $("#price-tracking-email").val();
                    var $this = $(this);
                    if (validateEmail(email)) {
                        $this.text('A moment please...');
                        $("#pn-price-tracker-modal-error").hide();
                        //open new tab first
                        window.open(url);
                        sendAjaxToPriceTracking(pnAjax.ajaxurl, url, email, productName, 'yes', function () {
                            $(".pn-price-tracking-modal-thanks-body").show();
                            $(".pn-price-tracking-modal-main-body").hide();
                            //$this.text('Yes Please');
                        });
                    }
                    else {
                        $("#pn-price-tracker-modal-error").show();
                    }
                });
            }else if($(this).hasClass('pn-mobile-link')){
                console.log(app_url);
                if(app_url != '' ) { // only prevent default click if app_url is defined
                    window.open(app_url);
                    sendAjaxToPriceTracking(pnAjax.ajaxurl, app_url, null, null, 'click', null);
                    e.preventDefault();
                    return;
                }
                else {
                    sendAjaxToPriceTracking(pnAjax.ajaxurl, url, null, null, 'click', null);
                    return ; // stop execution of script
                }

                // get rel and target attr of buttons and append to current button
                var btnAttr = $(this).clone();
                var rel = $(btnAttr).attr('rel');
                var target = $(btnAttr).attr('target');
                // set the href or url, target and rel
                $('#pn-app-yes').attr('href',app_url).attr('rel',rel).attr('target',target);
                $('#pn-app-no').attr('href',url).attr('rel',rel).attr('target',target);

                $.magnificPopup.open({
                    items: {
                        src: '#pn-link-modal',
                        type: 'inline'
                    },

                    fixedContentPos: false,
                    fixedBgPos: true,
                    overflowY: 'auto',
                    alignTop: true,
                    closeBtnInside: true,
                    preloader: false,
                    midClick: true,
                    removalDelay: 300,
                    mainClass: 'my-mfp-zoom-in'
                });
            } else {
                sendAjaxToPriceTracking(pnAjax.ajaxurl, url, null, null, 'click', null);
            }
        });
	})

})( jQuery );

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


function isMobile(){
    // check if mobile
    var isMobile = false; //initiate as false
    // device detection
    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4)))
    isMobile = true;

    return isMobile;
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
