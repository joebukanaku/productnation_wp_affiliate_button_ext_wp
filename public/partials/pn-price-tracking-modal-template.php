<?php
/**
*   {Tag} PN Custom
*   Add a comment to this line
*   - Link Modal Popup template
*/
?>
<div id='pn-price-tracking-modal' class='zoom-anim-dialog mfp-hide pn-modal'>
    <img class="logo" src="<?php echo plugin_dir_url(__FILE__). "../img/price-tracker-pn.png" ?>" />
    <p></p>
    <div class="pn-price-tracking-modal-main-body">
        <p class="pn-price-tracker-modal-product-name">Royal McQueen Hard Case</p>
<!--        <p class="pn-price-tracker-modal-description">Would you like to be alerted when the price of this item drops?</p>-->
        <span class="pn-price-tracker-modal-description">Would you like to be alerted when the <span style="color: #9b59b6;font-weight: bold">price</span> of this item drops?</span><br>
        <p></p>
        <input placeholder="Your Email" class="input-email-price-tracking" name="price-tracking-email" id="price-tracking-email" type="email">
        <p id="pn-price-tracker-modal-error" style="color: red;display: none">Invalid Email Address</p>
        <p></p>
        <div id="pn-link-btn-placement">
            <button  id="pn-price-tracking-yes" class="fasc-button fasc-size-medium fasc-type-flat pn-link-btn-yes"> Yes Please </button>
            <p></p>
            <button id="pn-price-tracking-no" class="fasc-button fasc-size-medium fasc-type-flat pn-link-btn-no"> No Thank You </button>
        </div>
        <p style="color: #9b59b6;font-weight: bold;">You will be directed to the product page*</p>
    </div>
    <div style="display: none" class="pn-price-tracking-modal-thanks-body">
        <p class="pn-price-tracker-modal-thankyou">Thank You</p>
        <p style="font-size:1.5em" class="pn-price-tracker-modal-thankyou">We will alert you when the price drops</p>
    </div>
</div>
