<?php

/**
 * Fired during plugin activation
 *
 * @link       http://codeamp.io
 * @since      1.0.0
 *
 * @package    Forget_About_Shortcode_Buttons
 * @subpackage Forget_About_Shortcode_Buttons/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Forget_About_Shortcode_Buttons
 * @subpackage Forget_About_Shortcode_Buttons/includes
 * @author     Ross <r@r.com>
 */
class Forget_About_Shortcode_Buttons_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate()
    {
        global $wpdb;
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        self::createPriceTrackingSurvey($wpdb);
	    self::createClickTracking($wpdb);
    }

    public static function createPriceTrackingSurvey($wpdb)
    {
        $table = $wpdb->prefix . 'price_tracking_surveys';

        $charset_collate = $wpdb->get_charset_collate();

        if ($wpdb->get_var("SHOW TABLES LIKE '$table'") != $table)
        {
            $sql = "CREATE TABLE `$table` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(11) COLLATE utf8mb4_unicode_520_ci DEFAULT 'yes',
  `produt_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `destination_url` text COLLATE utf8mb4_unicode_520_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `produt_name` (`produt_name`(191)),
  KEY `email` (`email`(191)),
  KEY `created_at` (`created_at`),
  KEY `type` (`type`),
  KEY `url` (`url`(191))
) ENGINE=InnoDB AUTO_INCREMENT=1 $charset_collate;";
            dbDelta($sql);
        }
    }

	public static function createClickTracking($wpdb)
	{
		$table = $wpdb->prefix . 'click_tracking';

		$charset_collate = $wpdb->get_charset_collate();

		if ($wpdb->get_var("SHOW TABLES LIKE '$table'") != $table)
		{
			$sql = "CREATE TABLE `$table` (
  `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) unsigned NOT NULL,
  `taxonomy_id` int(11) unsigned NOT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `source` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `destination_url` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `browser_name` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `browser_version` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `device_os` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_mobile` tinyint(1) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  KEY `taxonomy_id` (`taxonomy_id`),
  KEY `ip_address` (`ip_address`),
  KEY `source` (`source`(191)),
  KEY `destination_url` (`destination_url`(191)),
  KEY `country` (`country`),
  KEY `browser_name` (`browser_name`),
  KEY `browser_version` (`browser_version`),
  KEY `device_os` (`device_os`),
  KEY `is_mobile` (`is_mobile`),
  KEY `created_at` (`created_at`)
) ENGINE=InnoDB AUTO_INCREMENT=1 $charset_collate;";
			dbDelta($sql);
		}
	}

}
