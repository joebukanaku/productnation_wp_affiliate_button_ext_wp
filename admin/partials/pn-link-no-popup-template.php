<?php
    /**
    *   {Tag} PN Custom
    *   v1.1
    *   If has app_url in URL params, show this page
    *   If don't have app_url in URL param, redirect to the url
    */
    if( !isset($_REQUEST['app_url']) || $_REQUEST['app_url'] == '' || $_REQUEST['app_url'] == 'undefined'){
        // redirect user here
        if($_REQUEST['url']){
            header('Location:'. $_REQUEST['url']);
        }
        else {
            header('Location: http://productnation.co');
        }
        exit;
    }

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <div id="pn-no-script">
        <img class="logo" src="<?php echo plugin_dir_url(__FILE__). "../../public/img/productnation.png" ?>" />
        <div class="pn-link-modal-title">
            <?php
                echo wp_unslash( get_option( 'pn_link_modal_text' )  );
            ?>
        </div>
        <div id="pn-link-btn-placement">
            <a id="pn-app-yes" href="<?php echo $_REQUEST['app_url'] ?>" class="fasc-button fasc-size-medium fasc-type-flat pn-link-btn"> Yes </a>
            <a id="pn-app-no" href="<?php echo $_REQUEST['url'] ?>" class="fasc-button fasc-size-medium fasc-type-flat pn-link-btn"> No </a>
        </div>
    </div>
	<?php wp_footer(); ?>

</body>
</html>
